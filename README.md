# docker-compose-next

docker-compose setup for Next.js

Use
[ansible-role-docker-compose](https://gitlab.com/naturalis/lib/ansible/ansible-role-docker-compose)
to start and manage your docker-compose application using Ansible.
